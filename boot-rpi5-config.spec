%define debug_package %{nil}

Name:           boot-rpi5-config
Version:        0
Release:        5%{?dist}
Summary:        Package that configures direct kernel boot on a Raspberry Pi 5
License:        MIT

Source0:        config.txt
Source1:        99-vc4.conf
    
BuildRequires:  efi-filesystem
BuildRequires:  efi-srpm-macros

Requires:       bcm2712-firmware

Requires:       kernel-16k

Conflicts:      edk2-rpi5

%description
Simple package that installs the right files in the right places to allow a Raspberry Pi 5 to boot with kernel direct boot

%prep

%build

%install
mkdir -p %{buildroot}%{efi_esp_root}
install -m 0600 %{SOURCE0} %{buildroot}%{efi_esp_root}
mkdir -p %{buildroot}%{_sysconfdir}/X11/xorg.conf.d
install %{SOURCE1} %{buildroot}%{_sysconfdir}/X11/xorg.conf.d

%post
# Create cmdline.txt with grubby
if [ ! -f %{efi_esp_root}/cmdline.txt ]
then
    echo "root="$(grubby --info=$(grubby --default-kernel) | grep root= | cut -d"\"" -f 2) \
    $(grubby --info=$(grubby --default-kernel) | grep args | cut -d"\"" -f 2) \
        > %{efi_esp_root}/cmdline.txt
fi

# Copy the kernel and initramfs
ver=$(grubby --info=$(grubby --default-kernel) | grep kernel=\" | cut -d"\"" -f 2 | cut -d"-" -f 2)
rel=$(grubby --info=$(grubby --default-kernel) | grep kernel=\" | cut -d"\"" -f 2 | cut -d"-" -f 3)
vmlinuz=/boot/vmlinuz-$ver-$rel
initramfs=/boot/initramfs-$ver-$rel.img
install $vmlinuz %{efi_esp_root}/vmlinuz
install $initramfs %{efi_esp_root}/initramfs

%files
%config(noreplace) %{efi_esp_root}/config.txt
%{_sysconfdir}/X11/xorg.conf.d/99-vc4.conf

%changelog
* Tue Sep 24 2024 Luca Magrone <luca@magrone.cc> - 0-5
- Require the 16k page size kernel

* Sun May 05 2024 Luca Magrone <luca@magrone.cc> - 0-4
- rpm: add conflict with edk2-rpi5 package

* Wed Nov 22 2023 Luca Magrone <luca@magrone.cc> - 0-3
- Add X11 configuration file

* Sat Nov 18 2023 Luca Magrone <luca@magrone.cc> - 0-2
- Enable vc4 in config.txt

* Tue Nov 14 2023 Luca Magrone <luca@magrone.cc> - 0-1
- Initial package release
